BDSM
====

Abkürzung für Bondage & Discipline, Dominance & Submission und Sadism &
Masochism und ist daher eine Art Zusammenfassung verschiedener, aber trotzdem
ähnlicher, Spielarten.

-------------------------------------------------------------------------------

bondage
=======

Bezeichnet allgemeine Fesselspiele, sagt jedoch nichts über die Ausführung aus.
So kann Bondage einfach und funktional sein (z.B. Hände zusammenbinden), aber
auch sehr ästhetisch. Ein Beispiel für eine kunstvolle ausführung wäre
https://en.wikipedia.org/wiki/File:French_shibari_by_jerome_gouvrion.jpg
Gleichzeitig gibt es sehr unterschiedliche Materialien, wobei Seile und
Manschetten wohl die häufigsten Utensilien sind. Desweiteren gibt es auch etwas
intensivere Teilformen des Bondage, wie bspw. Mummifizierungen.

siehe dazu auch:
* [mummification]
* [shibari]

-------------------------------------------------------------------------------

mummification
=============

Hierbei wird der Körper des passiven Partners komplett bewegungsunfähig gemacht,
wobei es auch unterschiedlich starke Ausprägungen davon gibt: In der simpelsten
Variante bindet man die Arme inkl. der Oberarme an den Körper und die Füße inkl
Schenkel zusammen.

Kompliziertere Fesselungen können dann auch die Füße und die Finger bzw. die
Hand einschließen, was meistens das Gefühl der Hilflosigkeit nochmal verstärkt.

An Materialien ist "Bondagetape" zu empfehlen, welches nur auf sich selbst
haftet und etwas Luft durchlässt. Das hat - im Gegensatz zu der ebenso gern
verwendeten Frischhaltefolie - den Forteil, dass der Körper während der
Mummifizierung nicht überhitzt.


Weiterhin ist auf jedenfall eine stumpfe(!) Verbandsschere o.ä. zu empfehlen,
damit man die passiven Partners nicht verletzt.

siehe dazu auch:
* [bondage]

-------------------------------------------------------------------------------

shibari
=======

Eine ursprünglich aus Asien stammende Technik des erotischen Fesseln.
Praktizierende legen dabei einen großen Fokus auf Ästhetik.

Traditionellerweise werden für Shibari Naturseile verwendet.

siehe dazu auch:
* [bondage]

-------------------------------------------------------------------------------

suspension
==========

Bondage, die zumindest zu einem Teil hängend stattfindet. Suspension ist daher
eine Unterkategorie des allgmeinen Bondage.

siehe dazu auch:
* [bondage]

-------------------------------------------------------------------------------

dominance & submission
======================

Eine Spielart, in der Machtaustausch im Mittelpunkt steht. Meist dominiert eine
Person eine andere allein mit ihrer Stimme oder mit ihrem Auftreten.  Das kann
auch subtil im Alltag durch Kleinigkeiten wie leichtes Necken, aber auch allein
im privaten Schlafzimmer stattfinden.  Dominanz kann sich aber auch natürlich in
vielen anderen Dingen wiederspriegeln, z.B.  durch festhalten, etwas an den
Haaren ziehen oder auch durch stundenlanges stimulieren des Subs ohne Orgasmus.


-------------------------------------------------------------------------------


aftercare
=========

die Nachsorge insbesondere vom Sub nach dem Spiel. Aftercare soll möglichst
nichts oder nur sehr wenig mit BDSM zu tun haben, es ist in erster Linie dazu
da, Sub aufzufangen und die Session ausklingen zu lassen. D.h. Sub soll hier
emotional und psychisch wieder in einen ausgeglichenen Zustand sein, das
Adrenalin der Session abbauen.  Gleichzeitig ist das Aftercare natürlich auch
für Top da. Auch für ihn/sie ist eine Session anspruchsvoll: Man möchte
natürlich das Spiel interessant gestalten, man trägt während der Session eine
sehr große Verantwortung. Es erfordert außerdem höchste Konzentration, die
Reaktionen von Subbi zu beurteilen - gerade in komplexeren Situationen.  Das
alles erzeugt eine sehr hohe psychische Belastung, die natürlich auch Top am
Ende der Session wieder entfernen muss. Normalerweise steigt das Bedürfnis nach
einem guten Aftercare mit der Intensität der Session.

Gewöhnlich unterscheiden sich auch die Vorlieben wie das Aftercare konkret
aussehen soll. Einige mögen zum Beispiel kuscheln und "für den anderen da sein",
andere brauchen erstmal ein bisschen Zeit für sich selbst. Eventuell kann auch
eine Kleinigkeit zum Essen/Trinken angeboten werden, oder zur Entspannung ein
gemeinsames Bad nehmen. Gemeinsam ist allerdings allen Vorlieben meist die
Wundversorgung.

Im Allgemeinen ist beschreiben jedoch die Worte "Ruhe" und "Entspannung"
das Aftercare sehr gut.




-------------------------------------------------------------------------------

24/7
====

während bei "normalem" Spielen das Machtgefälle irgendwann aufgelöst wird,
bleibt jenes hier immer erhalten und wird dementsprechend nicht aufgelöst.
Unabhängig davon können innerhalb des Tages/der Woche, etc. unterschiedliche
Intensitäten praktiziert werden. D.h. man kann durchaus schön zusammen Fernsehen
(mit dementsprechend geringem Machtanteil) während Top im nächsten Moment Subbi
in einen Käfig sperren kann.  Desweiteren ist es für Top natürlich weiterhin
Pflicht, sich um Subbi zu kümmern, d.h. aftercare zu betreiben.

Allerdings ist die Macht des Doms hier beschränkt, d.h. er kann keine
Lebenswichtigen Entscheidungen über Sub bestimmen.

-------------------------------------------------------------------------------

TPE
====

Beinhaltet ein noch intensiveres Machtgefälle als in einer 24/7 Spielbeziehung:
Top kann hier quasi über das gesamte Leben der sub bestimmen, sogar welche
beruflichen Wege sie/er/es einschlägt, ob sie ein eigenes Bankkonto hat, mit
welchen Menschen sie/er/es sich umgeben darf.  Natürlich ist jede TPE Beziehung
individuell und der Übergang zum 24/7 ist fließend. So kann man eine TPE
Beziehung durchaus auch mit Safewort spielen.

-------------------------------------------------------------------------------

Masochismus
===========

Definiert die Neigung vom Spüren von Schmerzen erregt zu werden, wobei die
Intensität stark variieren kann. Häufig gibt es auch eine Präferenz was die Art
der Schmerzen angeht: beispielsweise werden manche nur durch Peitschenhiebe
erregt, während sie es typischerweise verabscheuen, sich den Zeh irgendwo
anzuschlagen.

-------------------------------------------------------------------------------

Sadismus
========

siehe [Masochismus], wobei Sadismus eben die aktive Variante davon
darstellt. Das heißt, man empfindet Erregung beim Anblick von
Schmerzempfindungen anderer Personen.  Allerdings gilt auch hier: Häufig gibt es
eine Präferenz der Art des Schmerzes.


-------------------------------------------------------------------------------

Safewort
========

In einer Spielsituation hat man häufiger die Situation dass man ein "Nein" oder
"hör auf!" ausspricht, ohne es wirklich so zu meinen. Die Situation ist zwar
unangenehm (zum Beispiel wenn man gekitzelt wird und nicht entweichen kann)
allerdings noch weit davon entfernt wirklich schlimm zu sein.

Safewörter sind demenstprechend definierte Wörter - oder auch Zeichen, wie ein
klingendes Glöckchen - die eindeutig zeigen sollen, dass eine Unterbrechung
gewünscht ist.  Dabei muss die Unterbrechung nichts schlimmes sein und zum
Abbruch der Session führen, eventuell möchte die/der Sub auch nur kurz die Nase
putzen.

Safewörter wie den [Ampelcode] erlauben dabei eine etwas feinere Ausdrucksweise.
Ein weiterer Safewort-Klassiker ist "Mayday".

-------------------------------------------------------------------------------

Ampelcode
=========

Der Ampelcode sind eine kleine Sammlung von Safewörtern, die sich an die Farben
einer Ampel anlehnen und eine feinere Ausdrucksweise ermöglichen. Die
Wortbedeutung kann natürlich individuell festgelegt werden, wobei im Folgenden
eine als Beispiel zu sehen ist:

grün --> alles in Ordnung, Top kann weitermachen
gelb --> bitte langsamer, weniger intensiv.
rot  --> bitte sofort aufhören.

Siehe dazu auch [Safewort]

-------------------------------------------------------------------------------

Vanilla
=======

1. Bezeichnet eine Person, die kein BDSM praktiziert.
2. Bezeichnet Tätigkeiten von BDSM praktizierenden Personen, die nichts
mit BDSM zu tun haben. Beispiele: der alltägliche ("vanilla") Job,
Sex ohne BDSM -> "vanilla sex"

-------------------------------------------------------------------------------

smothering
==========

Smothering (engl für "abdeckend" bzw. "erstickend") stellt eine Unterkategorie
von [Breathplay] dar, wobei als Werkzeug häufig der eigene Körper
bzw. Körperteile benutzt werden.

Beispiele:

* [hand-over-mouth smothering]
* [facesitting]

-------------------------------------------------------------------------------

breathplay
==========

"Breathplay" -- im deutschen Raum primär Atemreduktion genannt -- beschreibt
generell eine Gruppe von Spielarten, bei der das Atmen der submissiven Person
bewusst eingeschränkt wird.

Siehe dazu auch:

* [smothering]
* [facesitting]


-------------------------------------------------------------------------------

facesitting
===========


"Facesitting" oder auch "Queening" genannt ist allgemein ein Begriff für eine
Tätigkeit, bei der die dominante Person sich auf das Gesicht der submissiven
Person setzt, entweder um die Atmung zu erschweren (dann zählt es zu
[Smothering]) oder um den oralen Zugang zu den Genitalien zu erleichtern/zu
erzwingen.


Siehe dazu auch:

* [smothering]
* [breathplay]


-------------------------------------------------------------------------------
